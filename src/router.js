import VueRouter from 'vue-router'

//components
import StartScreen from './components/Start/StartScreen.vue';
import NotFoundScreen from './components/NotFound/NotFoundScreen.vue';
import QuizDisplay from './components/Quiz/QuizDisplay.vue'
import ResultDisplay from './components/Result/ResultDisplay.vue'

//routes
const routes = [
    {
    path: '/',
    name: 'Start',
    component: StartScreen,
    alias: '/home',
},
{
    path: '/quiz',
    name: 'Quiz',
    component: QuizDisplay
},
{
    path: '*',
    name: '404',
    component: NotFoundScreen
},
{
    path: '/results',
    name: 'results',
    component: ResultDisplay

}]

const router = new VueRouter ({routes})

export default router