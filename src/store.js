import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    quizzez: [],
    currentQuizIndex: 0,
    userAnswer: [],
    correctWrongAnswer: [],
    timeSpent: 0,
  },
  mutations: {
    setQuiz(state, payload) {
        state.quizzez = [...state.quizzez, ...payload]
    },
    resetState(state){
      state.quizzez = [];
    }
  },
  actions: {
    setUserAnswers(state, payload) {
        state.userAnswer = [...state.userAnswer, ...payload]
    },
    setCorrectWrongAnswer(state, payload) {
      state.correctWrongAnswer = [...state.correctWrongAnswer, ...payload]
    },
    setTimeSpent(state, payload){
      state.timeSpent = [...state.timeSpent, ...payload]
    },
  },
  getters: { 
    correctAnswer: (state) => {
         return state.quizzez.map((results) => results.correct_answer)
    },
    questionQuiz: (state) => {
        return state.quizzez.map((results) => results.question)
    },
}
 
})

export default store