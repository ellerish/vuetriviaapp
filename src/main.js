import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import router from './router'
import store from './store'

//vue-bootstrap
import { BootstrapVue, BProgress } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'


Vue.component('b-progress', BProgress)


Vue.config.productionTip = false

//adding router to app
Vue.use(VueRouter)

// Make BootstrapVue available throughout the project
Vue.use(BootstrapVue)

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')




