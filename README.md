# Trivia Game

The second javascript assignment for the Noroff .NET upskill course winter 2021. This assignment is a Trivia game developed with Vue and open trivia db API. 

## Features

The user starts the quiz game by clicking on the "Start game" button or anywhere on the screen. The app fetches the quiz questions from the API. The quiz consist of 10 questions. Each question has 4 answer options the user can choose. Each question counts 10 points. The app counts all the correct answers along with how much time the user spent doing the quiz. When the user clicks an answer the quiz moves on to the next question. When the user has answered 10 questions the quiz is finished and the app will display the result screen. The result screen displays alle the question with the correct answer and the answer the user chose. The users time and score along with the maximum score is also displayed. The user can restart the quiz again by clicking the "Play again" button.

## Setup
<ul>
<li> Clone the repository </li>
<li> npm install </li>
<li> npm run serve </li>
<li> Open the path the app is running at. (Typically http://localhost:8080/) </li>
</ul>

## Authors
<ul>
<li>Elise Rishaug</li>
<li>Camilla Arntzen</li>
</ul>

## Assignment requirements

<p>&#10004; Create a new vue application using the CLI</p>
<p>&#10004; Use the Vue components to build an application </p>
<p>&#10004; Use the VueRouter to navigate between components</p>

##### Start screen
<p>&#10004; The app starts on a start screen </p>
<p>&#10004; Button or click anywhere to start the game </p>

##### Questions
<p>&#10004; The app fetches questions from the API when the game starts.</p>
<p>&#10004; The app displays only one question at a time. </p>
<p>&#10004; Has 4 buttons with each answer as the text. </p>
<p>&#10004; When a question is answered the app moves on to the next. </p>
<p>&#10004; When all questions are answered the app moves on to the result screen. </p>

##### Result screen
<p>&#10004; The result screen contains the questions with the correct answers and the answers the user chose. </p>

##### Scoring
<p>&#10004; Each question counts 10 points. </p>
<p>&#10004; The app counts all the correct answers and displays the users score on the result screen. </p>
